+++
title = "Episode α2: Industrial Control Systems and Content Distribution"
date = "2023-06-03"
+++

<div>
<audio crossorigin="anonymous" controls src="https://traffic.libsyn.com/82efe563-51b8-4ccb-96f1-523d160bb443/episode-2.ogg" type="audio/ogg" />
<source src="audio/episode-1.ogg" type="audio/ogg" />
<source src="audio/episode-1.mp3" type="audio/mpeg" />
</audio>
</div>
<a href="https://directory.libsyn.com/episode/index/id/26967864"><img src="../img/libsyn_48.webp" width=48px></a>
<a href="https://soundcloud.com/lock-podcast"><img src="../img/soundcloud.png" width=48px></a>


Welcome to The Lock Podcast, where I explore technology and information security topics. This is the second alpha (α) episode of The Lock Podcast, or 🔒 ("The Lock.") For every episode I choose a few topics that may interest you, the listener, and I provide follow-up links at "lock podcast dot com", in case you want to know more. I do sincerely hope you all enjoy the show.
Episode α2 includes:
- Industrial control systems attacks,
- Residential Botnets with Zyxel and Mirai,
- last week's major vulnerabilities,
- Recent and Upcoming Hacker and Security Conferences,
- and building a resilient website

<!-- more -->

I'm M.J., and here is the news.

Top tech news has the Nintendo video game Zelda: Tears of the Kingdom continuing to amuse fans, and has issued a patch correcting a duplication bug. At the same time, the Japanese moon lander Hakuto or White Rabbit, appears to have taken an unfortunate three-mile drop at 100 meters per second into the moon due to a software glitch failing to account for the new landing site, according to Hakamada officials. Lastly, after settling complaints about animal welfare violations in its work, Elon Musk's firm Nuralink has obtained FDA approval for human brain implants.

In industrial news, Microsoft reports that the threat agent Volt Typhoon has been targetting critical United States infrastructure using standard living-off-the-land techniques. While at the same time, CISA has added another industrial control system vulnerability to its catalog of known exploits.

This week's Top Information Security news, Cisco Talos reports the increased use of the Intellexa Predator malware. This research is an expansion of Google's Threat Analysis Group (TAG) work from May 2022, that resulted in the published article "Protecting Android users from 0-Day Attacks". The Mirai botnet has also been upgraded to include multiple Zyxel remote buffer overflow vulnerabilities. CVEs 2023-33009 and 33010 were issued according to the Zyxel advisory.

Last week was Security B-Sides Budapest, Roanoke, Dublin, ExploitCon Boise, and Security Fest in Gothenburg. This coming week is x33fcon, Headwear.io USA, BSidesBuffalo, and CONFidence. Links to more information about the conferences is available at lockpodcast dot com slash events.

Finally, over the years, I've iterated through several hosting styles. A lot of VPS', some racked servers in data centers, and too many rented servers to count. All to get the uptime and latency that others were eventually selling at a far lower price than I could manage, with the sole advantage of "doing it myself." I did participate in a couple cooperatives that aimed to do it ourselves but eventually we all fell victim to AWS, Azure, OCI or Google Cloud. The reality is that what used to be measured in servers or packages is now measured in services or providers. Striking a balance between giving up control and uptime was a challenge. The DNS for this site lives on CloudNS, which chooses either Bunny.net or OpenBSD.Amsterdam to send traffic to, and those each serve up static HTML generated with the Rust tool Zola a decendant of Hugo. Both Bunny.net's Edge Rules and OpenBSD's RelayD Response Headers allow for setting Response Headers that are fun to get graded at Security Headers.com, as often it is a trial-and-error to get them right. Testing what has been built as it is being constructed is essential. I'll use services like Pingdom, WebPageTest, and PageSpeed.dev and do some of the ol' load testings before making them public with Let's Encrypt serving the TLS certificate.

Links to more information about all of the mentioned topics are available at [lockpodcast.com](https://www.lockpodcast.com/)


## Links and Notes

- [Microsoft: Volt Typhoon targets US critical infrastructure with living-off-the-land techniques](https://www.microsoft.com/en-us/security/blog/2023/05/24/volt-typhoon-targets-us-critical-infrastructure-with-living-off-the-land-techniques/)
- [Cisco Talos: Intellexa's Predator Malware](https://blog.talosintelligence.com/mercenary-intellexa-predator/)
- [CISA Adds another ICS vulnerability](https://www.cisa.gov/news-events/alerts/2023/05/26/cisa-adds-one-known-exploited-vulnerability-catalog)
- [G-TAG: Protecting Android users from 0-Day attacks](https://blog.google/threat-analysis-group/protecting-android-users-from-0-day-attacks/)
- [Nuralink](https://www.bbc.com/news/health-65717487)
- [Zyxel warns of critical vulnerabilities in Firewall and VPN Devices](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-critical-vulnerabilities-in-firewall-and-vpn-devices/)
- [Zyxel Advisory](https://www.zyxel.com/global/en/support/security-advisories/zyxel-security-advisory-for-multiple-buffer-overflow-vulnerabilities-of-firewalls)
- [Security B-Sides Roanoke](https://bsidesroa.org/)
- [Security Fest, Gothenburg](https://securityfest.com/)
- [InfoSec Conferences](https://infosec-conferences.com/category/hacker-conference/)
- [InfoSec Events by XSA](https://github.com/xsa/infosec-events)
- [x33fcon](https://www.x33fcon.com/)
- [Hardwear.io USA](https://hardwear.io/usa-2023/)
- [CONfidence CON](https://www.confidence-conference.org/)
- [OpenBSD.Amsterdam](https://www.openbsd.ams)
- [BunnyCDN](https://www.bunnycdn.net)
- [CloudNS](https://cloudns.net)
- [Zola](https://getzola.org)
- [Ransom Chats Viewer](https://ransomch.at)
