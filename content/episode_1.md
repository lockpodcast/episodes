+++
title = "Episode α1: Montana; Pre-installed Malware"
date = 2023-05-27
+++

<div>
<audio crossorigin="anonymous" controls src="https://traffic.libsyn.com/82efe563-51b8-4ccb-96f1-523d160bb443/episode-1.ogg" type="audio/ogg" />
<source src="audio/episode-1.ogg" type="audio/ogg" />
<source src="audio/episode-1.mp3" type="audio/mpeg" />
</audio>
</div>
<a href="https://directory.libsyn.com/episode/index/id/26967864"><img src="../img/libsyn_48.webp" width=48px></a>
<a href="https://soundcloud.com/lock-podcast"><img src="../img/soundcloud.png" width=48px></a>

Welcome to The Lock Podcast, exploring technology and information security topics. This is the first alpha (α) episode of The Lock Podcast, or 🔒 ("The Lock"), where I choose a few issues and events that seem noteworthy to me. Then, I bring them to you in a short format with links at "lock podcast dot com" if you want to know more. I do sincerely hope you all enjoy the show.
Episode α1 includes:
- the Montana TikTok ban and the response,
- last week's major vulnerabilities,
- Recent and Upcoming Hacker Conferences,
- and building a packet filter minefield on an OpenBSD router.

<!-- more --> 

I'm M.J., and here is the news.

Top tech news in the United States is that the Montana legislature has passed Senate Bill 419, approving a complete ban of the online, short video platform TikTok within the state. This bill, subtitled "Prohibiting a Mobile Application Store From Offering the TikTok Application to Montana Users," is being challenged as a constitutional First Amendment Free Speech matter by a coalition made up of the American Civil Liberties Union (ACLU), the Electronic Frontier Foundation (EFF), and other organizations.

Top Information Security news this week, CISA issued two Cisco IOS alerts due to overflow vulnerabilities and advisories for five industrial control systems. In consumer news, SANS Bites discussed Apple's released fixes for three vulnerabilities using their Rapid Security Response distribution method. Finally, as Trend Micro reported, specific OEM-packaged Android IoT devices are being pre-infected with Lemon Group malware.

Last week was THOTCON in Chicago and Security B-Sides Seattle and Fort Wayne, Indiana; and this week, Security B-Sides Budapest, Roanoke, Dublin, ExploitCon Boise, and Security Fest in Gothenburg. Links to more conferences are available at lockpodcast dot com slash events.

Finally, while updating my OpenBSD edge router, I found a link to "Tim's blog" in a comment—specifically, their aggressive pf configuration, which I modeled mine after. The idea is that systems have a handful of ports that services are listening on. So if an uninvited device attempts to connect to a network on multiple ports to check out what is available, you can safely take this as validation of malicious intent and drop further traffic from them. Accomplishing this combines triggering drop responses across a "minefield" of ports 1024 to 9999, adding caught agents to a troublemaker's table. For good measure, any popular service not offered but connected to should be tagged straight away as trouble. Stateful remains safe, of course, with egress traffic allowed back in while troublemakers coming in get dropped. Again, check out Tim's blog for more information and example pf configs.

Links to more information about all of the mentioned topics are available at [lockpodcast.com](https://www.lockpodcast.com/)

## Links and Notes

- [ACLU Slams Montana's Unconstitutional TikTok Ban as Governor Signs Law](https://www.aclu.org/press-releases/aclu-slams-montanas-unconstitutional-tiktok-ban-as-governor-signs-law)
- [Coalition Letter to Montana House of Representatives](https://www.aclu.org/documents/coalition-letter-opposing-montana-house-bill-that-would-ban-tiktok)
- [Government Hasn't Justified a TikTok Ban](https://www.eff.org/deeplinks/2023/03/government-hasnt-justified-tiktok-ban)
- [CISA Industrial Control Systems Advisories](https://www.cisa.gov/news-events/alerts/2023/05/18/cisa-releases-five-industrial-control-systems-advisories)
- [CISA Adds Three Known Exploited Vulnerabilities](https://www.cisa.gov/news-events/alerts/2023/05/19/cisa-adds-three-known-exploited-vulnerabilities-catalog) 
- [Lemon Group Cybercriminal Business Built on Pre-Infected Devices](https://www.trendmicro.com/en_us/research/23/e/lemon-group-cybercriminal-businesses-built-on-preinfected-devices.html)
- [ThotCon](https://thotcon.org/) 
- [Security B-Sides Seattle](https://www.bsidesseattle.com/2023-conference.html)
- [Security B-Sides Fort Wayne](https://bsidesfortwayne.org/)
- [Security B-Sides Roanoke](https://bsidesroa.org/)
- [Security Fest, Gothenburg](https://securityfest.com/)
- [InfoSec Conferences](https://infosec-conferences.com/category/hacker-conference/)
- [InfoSec Events by XSA](https://github.com/xsa/infosec-events)
- [BSides Budapest 2023](https://2023.bsidesbud.com/)
- [ExploitCon Boise 2023](https://exploitcon.com/#/)
- [BSides Dublin 2023](https://www.bsidesdub.ie)
- [Aggressive pf(4) configurations for SSH protection](https://blog.thechases.com/posts/bsd/aggressive-pf-config-for-ssh-protection/)

