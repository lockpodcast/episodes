# About


The Lock Podcast, or <a href="https://🔒podca.st/">🔒podca.st</a>, is a way of giving one to many education opportunities and information sharing in a short-form that allows the listener to follow up with the topics covered at their convenience.<br>
Features or longer episodes are being considered but right now I'm getting comfortable with the 3-5 minute form. 
<br>
<a href="https://🔒podca.st/">🔒podca.st</a> will feature a handful of topics mostly related to technology and information security news, education, and timely information disemination. 
<p>
<a href="https://🔒podca.st/">🔒podca.st</a> is also syndicated via: 

* [Lock.LibSyn.Com](https://lock.libsyn.com)
* [SoundCloud](https://soundcloud.com/lock-podcast)
* [Spotify](https://open.spotify.com/show/2Eo50s4UaylprxBSSGcqyw)

<center>
<a href="https://ransomch.at/"><img src="lock.png" height="128" width="128"></img></a>
</center>
