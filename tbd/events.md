# Events

The Lock Podcast, or <a href="https://🔒podca.st/">🔒podca.st</a>, is privileged to share with you timing of Hacker
and Information Security events around the globe. 

On the podcast when announcing events, they'll come from one of a few sources: <br>
	- [InfoSec Events by XSA](https://github.com/xsa/infosec-events) <br>
	- [Hacker Conferences](https://infosec-conferences.com/category/hacker-conference/) <br>
	- [Mastodon #infosec chatter](https://mastodon.social/tags/infosec) </p>


